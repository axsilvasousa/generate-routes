import React from "react";
import { Switch, Route, Link } from "react-router-dom";
import Senha from "./Senha";

const Rota = () => {
    return (
      <Switch>        
        <Route path="/senha" component={Senha} />
      </Switch>
    );
  };
  
  export default Rota;
  
