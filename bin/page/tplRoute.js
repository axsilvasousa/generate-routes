const content = (route, component) => `import React from "react";
import { Switch, Route, Link } from "react-router-dom";
import ${component} from "./${component}";

const Rota = () => {
    return (
      <Switch>        
        <Route path="/${route}" component={${component}} />
      </Switch>
    );
  };
  
  export default Rota;
  
`

module.exports = content
