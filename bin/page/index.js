const fs = require("fs")
const newFile = require("./tplFile")
const newRoute = require("./tplRoute")
const tplRoutes = require("./tplRoutes")
const _ = require("lodash")

const helper = () => {
    console.log(`Arguments:
    --component=Name of component
    --label=Description of menu
    --route=Link of menu and path of component
    [development] => --attr=Separate the attributes of menu with "," and replace "=" to ":"
        `)
}

const getArg = (argName, required = false) => {
    let argv = process.argv
    let item = argv.filter(i => {
        if (i.indexOf(argName) !== -1) {
            return true
        } else {
            return false
        }
    })
    if (required) {
        if (item.length == 0) {
            console.log(`This argument "${argName}" is required`)
            helper()
            process.exit()
        } else {
            return item[0].slice(item[0].indexOf("=") + 1)
        }
    } else if (item.length > 0) {
        return item[0].slice(item[0].indexOf("=") + 1)
    }

    return false
}
if (process.argv[2] == "-h" || process.argv[2] == "--helper") {
    helper()
    process.exit()
}
const nameComponent = getArg("--component", true)
const labelMenu = getArg("--label", true)
const linkMenu = getArg("--route", true).slice(1)
const cssFile = getArg("--css", false)
const attr = getArg("--attr", false)

const pathRoot = "src/pages/"
const pathDir = `${pathRoot}/${linkMenu}`
const routes = []
if (!fs.existsSync(pathDir)) {
    fs.mkdirSync(pathDir, 0744)
}
let contentNewFile = newFile(nameComponent, cssFile)
let contentNewRoute = newRoute(linkMenu, nameComponent)

const prepareAttr = atr => {
    if (atr !== false) {
        atr = atr.replace(":", "=")
        let attrs = atr.split(",")
        return attrs.join(" ")
    } else {
        return ""
    }
}
fs.writeFile(`${pathDir}/${nameComponent}.js`, contentNewFile, async function(err) {
    if (err) throw err
    if (cssFile !== false) {
        fs.writeFile(`${pathDir}/${cssFile}`, "", err => {
            if (err) console.log(err)
        })
    }
    await fs.writeFile(`${pathDir}/${nameComponent}Route.js`, contentNewRoute, async function(err) {
        await fs.readdirSync(pathRoot).forEach(async pathFile => {
            if (pathFile.slice(-3) !== ".js") {
                let path = `${pathRoot}/${linkMenu}`
                await fs.readdirSync(path).forEach(file => {
                    let m = file.match(/(.*[^Route])Route/i)
                    if (m !== null) {
                        let component = _.head(m)
                        routes.push({
                            label: `${labelMenu}`,
                            route: `/${linkMenu}`,
                            from: `./${linkMenu}/${component}`,
                            component,
                            attr: `'${prepareAttr(
                                "class:prett link color-white,style:{{width:100}}"
                            )}'`
                        })
                    }
                })
            }
        })

        let constRoutes = tplRoutes(routes)
        await fs.writeFile(`${pathRoot}/index.js`, constRoutes, async function(err) {
            if (err) throw err
            console.log(`Componente ${nameComponent} e sua rota foi gerado com sucesso!`)
        })
    })
})
