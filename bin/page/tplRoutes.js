let content = ""
const concat = txt => {
    content = content + txt
}
const route = routes => {
    routes.map(r => {
        concat(`import ${r.component} from "${r.from}" \n`)
    })

    concat(`const Routes = [`)
    routes.map((r, i) => {
        concat(`
    {
        label: "${r.label}",
        route: "${r.route}",
        component: ${r.component},
        attr:${r.attr}
    },\n`)
    })
    concat(`]\n export default Routes`)

    return content
}

module.exports = route
